﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ladner_Gatt.models
{
    class Gäddi
    {
        public string Vorname { get; set; }
        public string ZweiterVorname { get; set; }
        public string Nachname { get; set; }

        public override string ToString()
        {
            return "Gäddi Beschreibung" + " Vorname " + this.Vorname + " ZweiterVorname" + this.ZweiterVorname + " Nachname: " + this.Nachname + " Alter: " + this.Alter;
        }
    }
}
